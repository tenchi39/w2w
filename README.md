# W2W: What to watch

WHAT TO WATCH is a simple tool to help decide on a movie or television series to watch with friends or family. Each person can open it on their computer, tablet or phone and select a few entries from the list. If one or more entries are selected by all parties, it will be displayed as the common choice for the group.

## How does it work?

W2W reads your downloaded movie and TV series collection from your device and stores them in a database with additional data obtained from [themoviedb.org](https://themoviedb.org). The list is updated on the first run each day but can be initiated from the footer too.

## Requirements

For this to work, you need a minimal web server with PHP and MySQL to run the code on the device where you store your movies. You will also need an API key from [themoviedb.org](https://themoviedb.org) to query the data for the individual movies.

Your movie collection needs to be organized in subdirectories with proper naming as this will be the basis of the API query.

Movies  
&nbsp;&nbsp;|- Beat the Devil  
&nbsp;&nbsp;|- Blood on the Sun  
&nbsp;&nbsp;|- A Farewell to Arms  

You can also add the year the movie was released in brackets after the title to increase detection success.

## Installation

1. Extract the archive or clone the repo from bitbucket
2. Import the database.sql file to MySQL
3. Rename config-sample.php to config.php and fill out the needed entries
4. Open the installation directory in a browser