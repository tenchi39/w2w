<?php

/** MySQL hostname */
$db_host = '';

/** MySQL port (optional) - For MySQL the default port is 3306, for MariaDB it is 3307 */
$db_port = '';

/** The name of the database for W2W */
$db_database = '';

/** MySQL database username */
$db_user = '';

/** MySQL database password */
$db_password = '';

/** API key from themoviedb.org **/
$api_key = '';

/** Directory to parse for movies or tv series (Movies should be placed in subdirectories and properly named) **/
$movie_directory = '';
$tv_directory = '';

/** Keep it on false for personal use as it disabled database operations **/
$demo = false;

/** Mode **/
$default_mode = 'movie'; // can be 'movies' or 'tv'