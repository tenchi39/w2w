<?php

session_start();

// Import configuration
include('config.php');

// Check configuration
include('lib/check_config.php');

// Open MySQL connection
include('lib/database.php');

// Import necessary functions
include('lib/functions.php');

// Import animal images
include('lib/animals.php');

// Set listing order in the session
if($_SESSION['list'] == '') {
	$_SESSION['list'] = 'random';
}
if($_REQUEST['list'] == 'abc') {
	$_SESSION['list'] = 'abc';
}
if($_REQUEST['list'] == 'new') {
	$_SESSION['list'] = 'new';
}
if($_REQUEST['list'] == 'random') {
	$_SESSION['list'] = 'random';
}
if($_REQUEST['list'] == 'collection') {
	$_SESSION['list'] = 'collection';
}

// Set mode
if($_SESSION['mode'] == '') {
	$_SESSION['mode'] = $default_mode;
}

if ($_REQUEST['mode'] == 'tv') {
	$_SESSION['mode'] = 'tv';
	$_REQUEST['reset'] = true;
}

if ($_REQUEST['mode'] == 'movie') {
	$_SESSION['mode'] = 'movie';
	$_REQUEST['reset'] = true;
}


// Are we in demo mode?
if($demo == true) {
	// Reset the votes if more than 5 people used the demo page
	$result = mysqli_query($link, "SELECT DISTINCT votes_session FROM votes");
	if(mysqli_num_rows($result) > 5) {
		mysqli_query($link, "TRUNCATE TABLE votes");
		mysqli_query($link, "TRUNCATE TABLE blocked");	
		mysqli_query($link, "TRUNCATE TABLE animals");
	}
} else {
	// If there are no movies in the database, run the import function
	if(countMysqlItems($_SESSION['mode'], "WHERE ".$_SESSION['mode']."_title!=''") == 0) {
		$_REQUEST['import_movies'] = true;
	}

	// If the import function didn't run today, run it
	if(countMysqlItems('date', "WHERE date_date='".date("Ymd")."' AND date_mode='".$_SESSION['mode']."'") == 0) {
		$_REQUEST['import_movies'] = true;
		mysqli_query($link, "INSERT INTO date (date_date, date_mode) VALUES ('".date("Ymd")."', '".$_SESSION['mode']."')");
	}

	// Reset the database
	if($_REQUEST['reset_database'] == true) {
		resetDatabase();
		$reset_database_success = true;
		// Import movies
		$_REQUEST['import_movies'] = true;
	}
}

// Reset the current vote
if($_REQUEST['reset'] == true) {
	mysqli_query($link, "TRUNCATE TABLE votes");
	mysqli_query($link, "TRUNCATE TABLE blocked");
	mysqli_query($link, "TRUNCATE TABLE animals");
}

// Delete a chosen movie from the votes table for this user
if(is_numeric($_REQUEST['delete_vote'])) {
	mysqli_query($link, "DELETE FROM votes WHERE votes_movie='".$_REQUEST['delete_vote']."' AND votes_session='".session_id()."' LIMIT 1");
	$remove_success = true;
}

// Delete a blocked movie from the block list table for this user
if(is_numeric($_REQUEST['unblock'])) {
	mysqli_query($link, "DELETE FROM blocked WHERE blocked_movie='".$_REQUEST['unblock']."' AND blocked_session='".session_id()."' LIMIT 1");
	$unblock_success = true;
}

// Vote for a movie
if(is_numeric($_REQUEST['vote'])) {
	// If this movie is not present in the table for this user
	if(countMysqlItems('votes', "WHERE votes_movie='".$_REQUEST['vote']."' AND votes_session='".session_id()."'") == 0) {
		mysqli_query($link, "INSERT INTO votes (
			votes_movie, 
			votes_session
			) VALUES (
			'".$_REQUEST['vote']."', 
			'".session_id()."'
		)");
		$vote_success = true;
		// Also remove from the block list table for this user
		mysqli_query($link, "DELETE FROM blocked WHERE blocked_movie='".$_REQUEST['vote']."' AND blocked_session='".session_id()."'");
		// Set an animal for this user if there is none yet
		if(countMysqlItems('animals', "WHERE animals_session='".session_id()."'") == 0) {
			// Get all animals already in the table
			$results_animals = mysqli_query($link, "SELECT * FROM animals");
			// Remove these animals from the array
			while($myrow_animals = mysqli_fetch_assoc($results_animals)) {
				$animal_id = array_search($myrow_animals['animals_image'], $animals);
				unset($animals[$animal_id]);
			}
			$animal_image = array_rand($animals);
			// Insert animal image for this user in the database
			mysqli_query($link, "INSERT INTO animals (animals_image, animals_session) VALUES ('".$animals[$animal_image]."', '".session_id()."')");
		}
	}
}

// Block a movie
if(is_numeric($_REQUEST['block'])) {
	// If the movie is not present in the block list table for this user
	if(countMysqlItems('blocked', "WHERE blocked_movie='".$_REQUEST['block']."' AND blocked_session='".session_id()."'") == 0) {
		mysqli_query($link, "INSERT INTO blocked (
			blocked_movie, 
			blocked_session
			) VALUES (
			'".$_REQUEST['block']."', 
			'".session_id()."'
		)");
		$block_success = true;
		// Also remove from the vote table for this user
		mysqli_query($link, "DELETE FROM votes WHERE votes_movie='".$_REQUEST['block']."' AND votes_session='".session_id()."'");
	}
}

?><!doctype html>
<html class="no-js" lang="en" dir="ltr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>What to watch tonight?</title>
	<link rel="stylesheet" href="css/loading.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/css/foundation.min.css" integrity="sha256-ogmFxjqiTMnZhxCqVmcqTvjfe1Y/ec4WaRj/aQPvn+I=" crossorigin="anonymous">
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/motion-ui.min.css">
	<link rel="stylesheet" href="css/foundation-icons.css">

	<link rel="apple-touch-icon" sizes="57x57" href="apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="favicon-16x16.png">
	<link rel="manifest" href="manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
</head>
<body>
	<div class="loading" id="loading">Loading&#8230;</div>
	<div id="header">
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="small-12 cell" id="headercell">
					<b>
						<a href="index.php" class="hide-for-small-only">WHAT TO WATCH</a>
						<a href="index.php" class="show-for-small-only">W2W</a> 

						<span data-tooltip class="bottom" data-click-open="true" tabindex="2" title="WHAT TO WATCH is a simple tool to help decide on a movie or television series to watch with friends or family. Each person can open it on their computer, tablet or phone and select a few entries from the list. If one or more entries are selected by all parties, it will be displayed as the common choice for the group.">What is this?</span>
					</b>
					<?php

						// List all users who already voted
						$result = mysqli_query($link, "SELECT DISTINCT votes_session FROM votes");
						while($myrow = mysqli_fetch_assoc($result)) {
							// Get animal picture
							$result_animal = mysqli_query($link, "SELECT * FROM animals WHERE animals_session='".$myrow['votes_session']."' LIMIT 1");
							$myrow_animal = mysqli_fetch_assoc($result_animal);

							if($myrow['votes_session'] != session_id()) {
								echo '<a href="index.php?view_selection='.$myrow['votes_session'].'">';
							}
							echo '<span class="badge ';
							if($myrow['votes_session'] == session_id()) {
								echo 'alert';
							} else {
								echo 'primary';
							}
							echo ' float-right" title="';
							if($myrow['votes_session'] == session_id()) {
								echo 'Me';
							} else {
								echo 'Anonymous ';
								echo substr($myrow_animal['animals_image'], 0, -4);
							}
							echo '">';
							// Show how many movies each user voted for already
							echo countMysqlItems('votes', "WHERE votes_session='".$myrow['votes_session']."'");

							echo '<img src="img/animals/'.$myrow_animal['animals_image'].'" width="24" height="24" />';
							echo '</span>';
							if($myrow['votes_session'] != session_id()) {
								echo '</a>';
							}
						}
					?>
				</div>
			</div>
		</div>		
	</div>

	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<div class="small-12 cell">

				<?php

					// Show success message if the database was resetted
					if($reset_database_success == true) {
						echo '<div class="callout secondary fade-out" data-closable>';
						echo '<button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button>';
						echo '<p>Database reset successfully!</p>';
						echo '</div>';
					}
					// Update movie list and show results
					if($_REQUEST['import_movies'] == true) {
						$import_success = importMovies();
						if($import_success != '') {
							echo '<div class="callout fade-out" data-closable>';
							echo '<button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button>';
							echo '<p>Database updated!</p>';
							echo $import_success;
							echo '</div>';
						} elseif($api_error == true) {
							echo '<div class="callout fade-out" data-closable>';
							echo '<button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button>';
							echo '<p>Error! Could not get results from the API. Check $api_key in config.php</p>';
							echo '</div>';						

						} else {
							echo '<div class="callout fade-out" data-closable>';
							echo '<button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button>';
							echo '<p>No changes were detected!</p>';
							echo '</div>';						
						}
					}
					if($api_error == false) {
						// Show success message if the user voted for a movie
						if($vote_success == true) {
							echo '<div class="callout fade-out" data-closable>';
							echo '<button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button>';
							echo '<p>Item added: '.getMovie($_REQUEST['vote']).'</p>';
							echo '</div>';
						}
						// Show success message if the user voted for a movie
						if($remove_success == true) {
							echo '<div class="callout fade-out" data-closable>';
							echo '<button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button>';
							echo '<p>Item removed from selected: '.getMovie($_REQUEST['delete_vote']).'</p>';
							echo '</div>';
						}
						// Show a success message if the user blocked a movie
						if($block_success == true) {
							echo '<div class="callout fade-out" data-closable>';
							echo '<button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button>';
							echo '<p>Item blocked: '.getMovie($_REQUEST['block']).'</p>';
							echo '</div>';
						}
						// Show a success message if the user blocked a movie
						if($unblock_success == true) {
							echo '<div class="callout fade-out" data-closable>';
							echo '<button class="close-button" aria-label="Dismiss alert" type="button" data-close><span aria-hidden="true">&times;</span></button>';
							echo '<p>Item unblocked: '.getMovie($_REQUEST['unblock']).'</p>';
							echo '</div>';
						}

						// Count the number of users who already voted
						$result_concurrent_users = mysqli_query($link, "SELECT DISTINCT votes_session FROM votes");
						$concurrent_users = mysqli_num_rows($result_concurrent_users);
						// If more than one users voted for movies
						if($concurrent_users > 1) {
							// Initialize common movies array
							$common_movies = array();
							// Walk through all distinct selected movies
							$result_common = mysqli_query($link, "SELECT DISTINCT votes_movie FROM votes");
							while($myrow_common = mysqli_fetch_assoc($result_common)) {
								// If there is as many votes as concurrent users on the movie, we put the movie in the common movies array.
								if(countMysqlItems('votes', "WHERE votes_movie='".$myrow_common['votes_movie']."'") == $concurrent_users) {
									$common_movies[] = $myrow_common['votes_movie'];
								}
							}
							// If the common movies array is not empty
							if(!empty($common_movies)) {
								// echo '<div class="callout success">';
								echo '<h5>Co-selected movies</h5>';
								echo '<div class="grid-x grid-padding-x">';
								// Show the poster for the movies
								foreach ($common_movies as $value) {
									echo '<div class="small-4 medium-2 cell text-center">';
									$result = mysqli_query($link, "SELECT * FROM ".$_SESSION['mode']." WHERE ".$_SESSION['mode']."_id='".$value."' LIMIT 1");
									$myrow = mysqli_fetch_assoc($result);
									echo '<a href="index.php?show='.$myrow[$_SESSION['mode'].'_id'].'"><img src="https://image.tmdb.org/t/p/w500'.$myrow[$_SESSION['mode'].'_poster'].'" title="'.$myrow[$_SESSION['mode'].'_title'].'" /></a>';
									echo '<a href="index.php?delete_vote='.$myrow[$_SESSION['mode'].'_id'].'" class="button small expanded nomargin">Remove</a>';
									echo '</div>';
								}
								echo '</div>';
								// echo '</div>';
								echo '<br />';
							}
						}

						echo '<br>';


						// Query for alphabetical listing
						if($_SESSION['list'] == 'abc' or $_SESSION['list'] == 'collection') {
							$result = mysqli_query($link, "SELECT * FROM ".$_SESSION['mode']." ORDER BY ".$_SESSION['mode']."_title");
						}
						// Query for listing by date added
						if($_SESSION['list'] == 'new') {
							$result = mysqli_query($link, "SELECT * FROM ".$_SESSION['mode']." ORDER BY ".$_SESSION['mode']."_id DESC");
						}
						// Query for random listing
						if($_SESSION['list'] == 'random') {
							$filter = '';
							// List all movies the user already voted for as we do not want to show them in the random list
							$result_my_choice = mysqli_query($link, "SELECT * FROM votes WHERE votes_session='".session_id()."'");
							while($myrow_my_choice = mysqli_fetch_assoc($result_my_choice)) {
								if($_REQUEST['show'] != $myrow_my_choice['votes_movie']) {
									if($filter != '') {
										$filter .= ' AND ';
									}
									$filter .= $_SESSION['mode']."_id!='".$myrow_my_choice['votes_movie']."'";
								}
							}
							// List all movies the user already blocked as we do not want to show them in the random list
							$result_my_blocked = mysqli_query($link, "SELECT * FROM blocked WHERE blocked_session='".session_id()."'");
							while($myrow_my_blocked = mysqli_fetch_assoc($result_my_blocked)) {
								if($_REQUEST['show'] != $myrow_my_blocked['blocked_movie']) {
									if($filter != '') {
										$filter .= ' AND ';
									}
									$filter .= $_SESSION['mode']."_id!='".$myrow_my_blocked['blocked_movie']."'";
								}
							}
							if($filter == '') {
								// Query for random listing if there are no chosen or blocked movies
								$result = mysqli_query($link, "SELECT * FROM ".$_SESSION['mode']." ORDER BY rand()");
							} else {
								// Query for random listing if there are chosen or blocked movies
								$result = mysqli_query($link, "SELECT * FROM ".$_SESSION['mode']." WHERE $filter ORDER BY rand()");
							}
						}
						if($_SESSION['list'] != 'collection' and $_REQUEST['view_selection'] == '') {
							// Carousel
							echo '<div class="orbit" role="region" aria-label="Items" data-orbit data-auto-play="false" data-options="animInFromLeft:fade-in; animInFromRight:fade-in; animOutToLeft:fade-out; animOutToRight:fade-out;">';
							echo '<div class="orbit-wrapper">';
							echo '<div class="orbit-controls">';
							echo '<button class="orbit-previous">&lt;</button>';
							echo '<button class="orbit-next">&gt;</button>';
							echo '</div>';
							echo '<ul class="orbit-container">';

							// Set a variable to check if the current item is the first item in the movie list
							$first = true;
							while($myrow = mysqli_fetch_assoc($result)) {
								echo '<li class="';
								// Make the first slide active if there was no vote and no specific movie is to be shown
								if($first == true and $_REQUEST['vote'] == '' and $_REQUEST['show'] == '') {
									echo 'is-active ';
								}
								// Make the movie slide active if it was voted for
								if($_REQUEST['vote'] == $myrow[$_SESSION['mode'].'_id']) {
									echo 'is-active ';
								}
								// Make the movie slide active if it was requested to be shown
								if($_REQUEST['show'] == $myrow[$_SESSION['mode'].'_id']) {
									echo 'is-active ';
								}
								echo 'orbit-slide">';
								echo '<div class="moviecard" style="background-image: url(https://image.tmdb.org/t/p/original'.$myrow[$_SESSION['mode'].'_backdrop'].');">';
								echo '<div class="movieopacity">';
								echo '<div class="grid-x grid-padding-x">';
								echo '<div class="small-10 medium-4 large-3 small-offset-1 medium-offset-1 cell text-center">';
								echo '<img src="https://image.tmdb.org/t/p/w500'.$myrow[$_SESSION['mode'].'_poster'].'" />';
								// Show button that helps searching for a trailer on Youtube
								echo '<a class="button expanded nomargin secondary" href="https://www.youtube.com/results?search_query='.urlencode($myrow[$_SESSION['mode'].'_title']).'+trailer" target="_blank"><i class="fi-play"></i> Play trailer on Youtube</a>';
								// Show the vote or remove button
								if(countMysqlItems('votes', "WHERE votes_movie='".$myrow[$_SESSION['mode'].'_id']."' AND votes_session='".session_id()."'") == 0) {
									echo '<a class="button expanded nomargin" href="index.php?vote='.$myrow[$_SESSION['mode'].'_id'].'"><i class="fi-plus"></i> Add to selected items</a>';
								} else {
									echo '<a href="index.php?delete_vote='.$myrow[$_SESSION['mode'].'_id'].'" class="button expanded nomargin"><i class="fi-x"></i> Remove from selected items</a>';
								}
								// Show the block or unblock button
								if(countMysqlItems('blocked', "WHERE blocked_movie='".$myrow[$_SESSION['mode'].'_id']."' AND blocked_session='".session_id()."'") == 0) {
									echo '<a href="index.php?block='.$myrow[$_SESSION['mode'].'_id'].'" class="button expanded margin_bottom alert"><i class="fi-prohibited"></i> Block from list</a>';
								} else {
									echo '<a href="index.php?unblock='.$myrow[$_SESSION['mode'].'_id'].'" class="button expanded margin_bottom alert"><i class="fi-x"></i> Remove from blocked items</a>';
								}
								echo '</div>';
								echo '<div class="small-12 medium-6 large-7 cell movietext hide-for-small-only">';
								echo '<h1>'.$myrow[$_SESSION['mode'].'_title'].'</h1>';
								echo '<div class="movieoverview">';
								echo '<p>Rating: <b><big>'.$myrow[$_SESSION['mode'].'_rating'].' <i class="fi-star"></i></big></b></p>';
								echo '<p>'.$myrow[$_SESSION['mode'].'_overview'].'</p>';
								echo '<p>'.$myrow[$_SESSION['mode'].'_release_date'].'</p>';
								echo '</div>';
								echo '</div>';
								echo '</div>';
								echo '</div>';
								echo '</div>';
								echo '</li>';
								// After the first slide set the $first variable to false
								$first = false;
								echo "\n";
							}
							echo '</ul>';
							echo '</div>';
							echo '</div>';
						} else if ($_SESSION['list'] == 'collection' and $_REQUEST['view_selection'] == '') {
							// Show the whole collection
							echo '<div class="float-right">Total: ';
							echo countMysqlItems($_SESSION['mode'], "WHERE ".$_SESSION['mode']."_id!=''");

							echo '</div>';
							echo '<h5>Collection:</h5>';


							echo '<div class="grid-x grid-padding-x">';
							while($myrow = mysqli_fetch_assoc($result)) {
								echo '<div class="small-6 medium-2 cell text-center">';
								echo '<a href="index.php?show='.$myrow[$_SESSION['mode'].'_id'].'&amp;list=abc"><img src="https://image.tmdb.org/t/p/w500'.$myrow[$_SESSION['mode'].'_poster'].'" title="'.$myrow[$_SESSION['mode'].'_title'].'" />';
								echo '<br>';
								echo '<br>';
								echo $myrow[$_SESSION['mode'].'_title'].'</a>';
								echo '<br>';
								echo '<br>';
								echo '</div>';
							}
							echo '</div>';
						} else {
							// Show the choices of an anonymous animal
							$result_animal = mysqli_query($link, "SELECT * FROM animals WHERE animals_session='".$_REQUEST['view_selection']."' LIMIT 1");
							$myrow_animal = mysqli_fetch_assoc($result_animal);
							echo '<h3>';
							echo 'Anonymous ';
							echo substr($myrow_animal['animals_image'], 0, -4);
							echo '&nbsp;';
							echo '<img src="img/animals/'.$myrow_animal['animals_image'].'" width="36" height="36" />';
							echo '</h3>';
							echo listChoises($_REQUEST['view_selection'], false);
							echo listBlocked($_REQUEST['view_selection'], false);
						}
						if($_REQUEST['view_selection'] == '') {
							// Show the movies the user voted for
							echo listChoises(session_id(), true);
							// Show the movies the user blocked
							echo listBlocked(session_id(), true);
						}
					}

				?>
				<br>

				<div id="footer">

				Mode: 
				<span class="nowrap">[ <a href="index.php?mode=movie">Movies</a> ]</span>
				<span class="nowrap">[ <a href="index.php?mode=tv">Series</a> ]</span>
				<span class="hide-for-small-only">&nbsp;&nbsp;&nbsp;</span>
				<span class="show-for-small-only"><br></span>
				Sorting:
				<span class="nowrap">[ <a href="index.php?list=random">Random</a> ] </span>
				<span class="nowrap">[ <a href="index.php?list=abc">ABC</a> ] </span>
				<span class="nowrap">[ <a href="index.php?list=new">New</a> ]</span>
				<span class="nowrap">[ <a href="index.php?list=collection">All</a> ]</span>
				<br>
				<span class="nowrap">[ <a href="index.php?reset=true" onclick="return confirm('Are you sure?')">Reset votes</a> ]</span>
				<?php
					if($demo == true) {
						echo '<span class="nowrap">[ <a href="#" onclick="alert(\'Disabled in demo mode\');">Update DB</a> ]</span>';
						echo "\n";
						echo '<span class="nowrap">[ <a href="#" onclick="alert(\'Disabled in demo mode\');">Reset DB</a> ]</span>';
					} else {
						echo '<span class="nowrap">[ <a href="index.php?import_movies=true" onclick="return confirm(\'Are you sure?\')">Update DB</a> ]</span>';
						echo "\n";
						echo '<span class="nowrap">[ <a href="index.php?reset_database=true" onclick="return confirm(\'Are you sure?\')">Reset DB</a> ]</span>';
					}
				?>
				<span class="hide-for-small-only">&nbsp;&nbsp;&nbsp;</span>
				<span class="show-for-small-only"><br></span>
				<span class="nowrap">[ <a href="README.md" target="blank">Read me</a> ]</span> 
				<span class="nowrap">[ <a href="https://bitbucket.org/tenchi39/w2w/src/master/" target="blank">Bitbucket project page</a> ]</span> 
				<span class="nowrap">[ <a href="https://randomblog.hu/about/" target="blank">Author</a> ]</span>

				</div>

			</div>
		</div>
	</div>

	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/foundation-sites@6.6.3/dist/js/foundation.min.js" integrity="sha256-pRF3zifJRA9jXGv++b06qwtSqX1byFQOLjqa2PTEb2o=" crossorigin="anonymous"></script>
	<script>

		$(document).ready(function() {
			$(document).foundation();
		});

		$(window).on("load", function() {
			$( "#loading" ).fadeOut( "slow", function() {
			});			
		});

	</script>
</body>
</html>
