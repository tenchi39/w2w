<?php

// Count items in a MySQL table based on the $where criteria
function countMysqlItems($table, $where) {
	global $link;
	$result = mysqli_query($link, "SELECT ".$table."_id FROM ".$table." ".$where);
    $counter = mysqli_num_rows($result);
	return $counter;
}

// Return a string between two strings
function getStringBetween($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

// List the contents of a directory
function getDirList($directory) {
	$filelist = array();
	$dir_handle = opendir($directory);
	while($file = readdir($dir_handle)) {
		if($file != '.' and $file != '..') {
			$filelist[] = $directory.'/'.$file;
		}
	}
	@closedir($dir_handle);
	return $filelist;
}

function getMovie($id) {
	global $link;
	$result = mysqli_query($link, "SELECT * FROM ".$_SESSION['mode']." WHERE ".$_SESSION['mode']."_id='".$id."' LIMIT 1");
	$myrow = mysqli_fetch_assoc($result);
	return '<a href="index.php?show='.$myrow[$_SESSION['mode'].'_id'].'">'.$myrow[$_SESSION['mode'].'_title'].'</a>';
}

// Truncate the database
function resetDatabase() {
	global $link;
	mysqli_query($link, "TRUNCATE TABLE movie");
	mysqli_query($link, "TRUNCATE TABLE tv");
	mysqli_query($link, "TRUNCATE TABLE votes");
	mysqli_query($link, "TRUNCATE TABLE blocked");
	mysqli_query($link, "TRUNCATE TABLE animals");
}

// Check if directory is empty
function is_dir_empty($dir) {
  if (!is_readable($dir)) return NULL; 
  return (count(scandir($dir)) == 2);
}

// Import movies from disk
function importMovies() {
	global $link, $api_key, $movie_directory, $tv_directory, $api_error;

	$text = '';

	if($_SESSION['mode'] == 'movie') {
		$directory = $movie_directory;
	} else {
		$directory = $tv_directory;
	}

	$dirList = getDirList($directory);
	asort($dirList);
	reset($dirList);
	unset($directories);

	$counter = 1;
	foreach ($dirList as $key => $value) {
		if(!is_dir_empty($value)) {
			$value = explode('/', $value);
			$directory_name = $value[count($value)-1];
			$directories[] = $directory_name;
			$title = explode('(', $directory_name);
			$year = $title[1];
			$year = substr($year, 0, -1);
			$title = $title[0];
			$title = trim($title);

			// Get data from themoviedb.org
			if(countMysqlItems($_SESSION['mode'], "WHERE ".$_SESSION['mode']."_title='".mysqli_real_escape_string($link, $title)."' LIMIT 1") == 0) {
				$moviedb_link = 'https://api.themoviedb.org/3/search/'.$_SESSION['mode'].'?api_key='.$api_key.'&language=en-US&page=1&include_adult=false&query='.urlencode($title).'&primary_release_year='.$year;
				;

				$poster_url_base = 'https://image.tmdb.org/t/p/w500/';

				$json = file_get_contents($moviedb_link);
				// if no result is recieved
				if($json == '') {
					$api_error = true;
					break;
				}

				if($json == '{"page":1,"results":[],"total_pages":0,"total_results":0}') {
					if($_SESSION['mode'] == 'movie') {
						$text .= '#'.$counter.' - Error, movie not found: '.$title.'<br />';
					} else {
						$text .= '#'.$counter.' - Error, series not found: '.$title.'<br />';
					}
				} else {
					$movie_original_title = getStringBetween($json, '"original_title":"', '"');
					$movie_poster = getStringBetween($json, '"poster_path":"', '"');
					$movie_backdrop = getStringBetween($json, '"backdrop_path":"', '"');
					$movie_overview = getStringBetween($json, '"overview":"', '"');
					$movie_release_date = getStringBetween($json, '"release_date":"', '"');
					$movie_rating = getStringBetween($json, '"vote_average":', ',"');
					mysqli_query($link, "INSERT INTO ".$_SESSION['mode']." (
						".$_SESSION['mode']."_title, 
						".$_SESSION['mode']."_original_title, 
						".$_SESSION['mode']."_directory, 
						".$_SESSION['mode']."_poster, 
						".$_SESSION['mode']."_backdrop, 
						".$_SESSION['mode']."_overview, 
						".$_SESSION['mode']."_release_date, 
						".$_SESSION['mode']."_rating
						) VALUES (
						'".mysqli_real_escape_string($link, $title)."', 
						'".mysqli_real_escape_string($link, $movie_original_title)."', 
						'".mysqli_real_escape_string($link, $directory_name)."', 
						'".mysqli_real_escape_string($link, $movie_poster)."', 
						'".mysqli_real_escape_string($link, $movie_backdrop)."', 
						'".mysqli_real_escape_string($link, $movie_overview)."', 
						'".mysqli_real_escape_string($link, $movie_release_date)."', 
						'".mysqli_real_escape_string($link, $movie_rating)."'
					)");
					$last_id = mysqli_insert_id($link);
					if($_SESSION['mode'] == 'movie') {
						$text .= '#'.$counter.' - Movie added: <a href="index.php?show='.$last_id.'&amp;list=abc">'.$title;
					} else {
						$text .= '#'.$counter.' - Series added: <a href="index.php?show='.$last_id.'&amp;list=abc">'.$title;
					}
					if($year != '') {
						$text .= ' ('.$year.')';
					}
					$text .= '</a>';
					$text .= '<br>';
				} 
			}
			$counter++;
		}
	}
	// Remove movies deleted from disk
	$result = mysqli_query($link, "SELECT ".$_SESSION['mode']."_id, ".$_SESSION['mode']."_directory FROM ".$_SESSION['mode']);
	while($myrow = mysqli_fetch_assoc($result)) {
		if(!in_array($myrow[$_SESSION['mode'].'_directory'], $directories)) {
			mysqli_query($link, "DELETE FROM ".$_SESSION['mode']." WHERE ".$_SESSION['mode']."_id='".mysqli_real_escape_string($link, $myrow[$_SESSION['mode'].'_id'])."' LIMIT 1");
			if($_SESSION['mode'] == 'movie') {
				$text .= 'Movie deleted: '.$myrow[$_SESSION['mode'].'_directory'].'<br />';
			} else {
				$text .= 'Series deleted: '.$myrow[$_SESSION['mode'].'_directory'].'<br />';
			}
		}
	}
	return $text;
}

// Show the movies the user voted for
function listChoises($sessionid, $controls) {
	global $link;
	if(countMysqlItems('votes', "WHERE votes_session='".$sessionid."'") != 0) {
		$text = '<br>';
		$text .= '<h5>Selected items:</h5>';
		$result_my_choice = mysqli_query($link, "SELECT * FROM votes WHERE votes_session='".$sessionid."'");
		$text .= '<div class="grid-x grid-padding-x">';
		while($myrow_my_choice = mysqli_fetch_assoc($result_my_choice)) {
			$text .= '<div class="small-4 medium-2 cell text-center">';
			$result = mysqli_query($link, "SELECT * FROM ".$_SESSION['mode']." WHERE ".$_SESSION['mode']."_id='".$myrow_my_choice['votes_movie']."' LIMIT 1");
			$myrow = mysqli_fetch_assoc($result);
			$text .= '<a href="index.php?show='.$myrow[$_SESSION['mode'].'_id'].'"><img src="https://image.tmdb.org/t/p/w500'.$myrow[$_SESSION['mode'].'_poster'].'" title="'.$myrow[$_SESSION['mode'].'_title'].'" /></a>';
			if($controls == true) {
				$text .= '<a href="index.php?delete_vote='.$myrow[$_SESSION['mode'].'_id'].'" class="button small expanded">Remove</a>';
			}
			$text .= '</div>';
		}
		$text .= '</div>';
		return $text;
	}
}

// Show the movies the user blocked
function listBlocked($sessionid, $controls) {
	global $link;
	if(countMysqlItems('blocked', "WHERE blocked_session='".$sessionid."'") != 0) {

		$text = '<br>';
		$text .= '<h5>Blocked items:</h5>';
		$result_my_choice = mysqli_query($link, "SELECT * FROM blocked WHERE blocked_session='".$sessionid."'");
		$text .= '<div class="grid-x grid-padding-x">';
		while($myrow_my_choice = mysqli_fetch_assoc($result_my_choice)) {
			$text .= '<div class="small-4 medium-2 cell text-center">';
			$result = mysqli_query($link, "SELECT * FROM ".$_SESSION['mode']." WHERE ".$_SESSION['mode']."_id='".$myrow_my_choice['blocked_movie']."' LIMIT 1");
			$myrow = mysqli_fetch_assoc($result);
			$text .= '<a href="index.php?show='.$myrow[$_SESSION['mode'].'_id'].'"><img src="https://image.tmdb.org/t/p/w500'.$myrow[$_SESSION['mode'].'_poster'].'" title="'.$myrow[$_SESSION['mode'].'_title'].'" /></a>';
			if($controls == true) {
				$text .= '<a href="index.php?unblock='.$myrow[$_SESSION['mode'].'_id'].'" class="button small expanded">Remove</a>';
			}
			$text .= '</div>';
		}
		$text .= '</div>';
		return $text;
	}
}