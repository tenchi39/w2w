<?php

// Check mode
if($default_mode == '') {
	$default_mode == 'movie';
} elseif($default_mode != 'movie' and $default_mode != 'tv') {
	echo 'Configuration error: Check the $default_mode variable in config.php';
	echo '<br>';
	exit();
}

// If not in demo mode, check if the directory and API key is filled out
if($demo == false) {
	if($movie_directory == '' and $tv_directory == '') {
		echo 'Configuration error: Check the $movie_directory and $tv_directory variables in config.php';
		echo '<br>';
		exit();
	}
	if($api_key == '') {
		echo 'Configuration error: Check the $api_key variable in config.php';
		echo '<br>';
		exit();
	}
}